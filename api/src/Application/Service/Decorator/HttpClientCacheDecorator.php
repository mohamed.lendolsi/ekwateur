<?php

namespace Application\Service\Decorator;

use Infrastructure\ApiClient\EkwateurClient\EkwateurAgentApi;
use Symfony\Contracts\Cache\CacheInterface;

class HttpClientCacheDecorator
{
    public function __construct(
        private readonly CacheInterface $fileSystemCache,
        private readonly EkwateurAgentApi $ekwateurAgentApi
    ) {
    }

    public function request(int $id): array
    {
        $cacheKey = 'http_client_' . md5($this->ekwateurAgentApi->getHttpMethode() . $this->ekwateurAgentApi->getUri());

        $cachedResponse = $this->fileSystemCache->getItem($cacheKey);
        if ($cachedResponse->isHit()) {
            return $cachedResponse->get();
        }

        $response = $this->ekwateurAgentApi->getAgentById($id);

        $cachedResponse->set($response)->expiresAfter(3600);
        $this->fileSystemCache->save($cachedResponse);

        return $response;
    }
}
