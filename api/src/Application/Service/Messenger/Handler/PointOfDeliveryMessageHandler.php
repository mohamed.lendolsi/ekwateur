<?php

namespace Application\Service\Messenger\Handler;

use Application\DTO\PointOfDeliveryAsyncDTO;
use Application\Service\Manager\PointOfDeliveryManager;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Application\Service\Messenger\Message\PointOfDeliveryMessage;

#[AsMessageHandler]
class PointOfDeliveryMessageHandler
{
    public function __construct(private PointOfDeliveryManager $pointOfDeliveryManager)
    {
    }
    public function __invoke(PointOfDeliveryMessage $deliveryMessage)
    {
        try {
            $pointOfDeliveryAsyncDTO =  PointOfDeliveryAsyncDTO::createFromPayload(json_decode($deliveryMessage->getContent(), true)) ;
            $this->pointOfDeliveryManager->updatePOD($pointOfDeliveryAsyncDTO);
        } catch (\Exception) {
            //reply the payload
            //Error Queue
        }
    }
}
