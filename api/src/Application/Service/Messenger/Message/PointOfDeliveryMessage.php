<?php

namespace Application\Service\Messenger\Message;

class PointOfDeliveryMessage
{
    public function __construct(
        private string $content,
    ) {
    }
    public function getContent(): string
    {
        return $this->content;
    }
}
