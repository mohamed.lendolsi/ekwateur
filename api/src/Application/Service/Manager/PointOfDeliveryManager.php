<?php

namespace Application\Service\Manager;

use Application\DTO\PointOfDeliveryAsyncDTO;
use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\PointOfDelivery;

class PointOfDeliveryManager
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }
    public function updatePOD(PointOfDeliveryAsyncDTO $pointOfDeliveryAsyncDTO)
    {
        $repository = $this->entityManager->getRepository(PointOfDelivery::class);
        $pointOfDeliveries = $repository->findBy(['reference' => $pointOfDeliveryAsyncDTO->reference]);

        if(empty($pointOfDeliveries)) {
            return;
        }

        foreach ($pointOfDeliveries as $pointOfDelivery) {
            $pointOfDelivery
                ->setInseeCode($pointOfDeliveryAsyncDTO->inseeCode)
                ->setCity($pointOfDeliveryAsyncDTO->city)
                ->setAddress($pointOfDeliveryAsyncDTO->address);

            $this->entityManager->persist($pointOfDelivery);
        }

        $this->entityManager->flush();
    }
}
