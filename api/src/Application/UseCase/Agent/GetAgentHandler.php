<?php

namespace Application\UseCase\Agent;

use Application\DTO\AgentDTO;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class GetAgentHandler
{
    private iterable $agentRepositories;
    public function __construct(#[TaggedIterator('agent.repository_tag')] iterable $agentRepositories)
    {
        $this->agentRepositories = $agentRepositories;
    }

    public function handle(int $id): AgentDTO
    {
        foreach ($this->agentRepositories as $agentRepository) {
            $agentDTO =  $agentRepository->findAgentById($id);
            if($agentDTO instanceof AgentDTO) {
                break;
            }
        }
        return $agentDTO;
    }
}
