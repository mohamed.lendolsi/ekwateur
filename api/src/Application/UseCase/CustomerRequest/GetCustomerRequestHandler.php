<?php

namespace Application\UseCase\CustomerRequest;

use Application\DTO\CustomerRequestDTO;
use Application\DTO\CustomerRequestEntityDTO;
use Application\UseCase\Agent\GetAgentHandler;
use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\CustomerRequest;

class GetCustomerRequestHandler
{
    public function __construct(private readonly GetAgentHandler $getAgentHandler)
    {
    }

    public function handle(CustomerRequest $customerRequest): CustomerRequestEntityDTO
    {
        $agentDTO = $this->getAgentHandler->handle($customerRequest->getAgent());

        return CustomerRequestEntityDTO::createFromEntity($customerRequest, $agentDTO);
    }
}
