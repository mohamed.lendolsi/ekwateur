<?php

namespace Application\UseCase\CustomerRequest;

use Application\DTO\CustomerRequestDTO;
use Application\UseCase\Agent\GetAgentHandler;
use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\CustomerRequest;

class CreateCustomerRequestHandler
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function handle(CustomerRequestDTO $customerRequestDTO): CustomerRequest
    {
        $customerRequestEntity = CustomerRequest::createFromDto($customerRequestDTO);
        $this->entityManager->persist($customerRequestEntity);
        $this->entityManager->flush();

        return $customerRequestEntity;
    }
}
