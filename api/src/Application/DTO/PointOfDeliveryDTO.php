<?php

namespace Application\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class PointOfDeliveryDTO
{
    public function __construct(
        #[Assert\Type('digit')]
        public readonly string $reference,
    ) {
    }
}
