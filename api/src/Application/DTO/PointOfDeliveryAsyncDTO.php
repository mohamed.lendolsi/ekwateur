<?php

namespace Application\DTO;

class PointOfDeliveryAsyncDTO
{
    public function __construct(
        public readonly int $reference,
        public readonly string $inseeCode,
        public readonly string $city,
        public readonly string $address,
    ) {
    }

    public static function createFromPayload(array $payload): self
    {
        return new self(
            reference:$payload['reference'] ?? '',
            inseeCode:$payload['inseeCode'] ?? '',
            city: $payload['city'] ?? '',
            address: $payload['address'] ?? '',
        );
    }
}
