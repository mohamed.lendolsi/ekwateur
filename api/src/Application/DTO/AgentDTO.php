<?php

namespace Application\DTO;

class AgentDTO
{
    public function __construct(
        public readonly string  $id,
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly string $email
    ) {
    }

    public static function createFormApiData(array $agentData): self
    {
        return new self(
            id: $agentData['id']??'',
            firstName: $agentData['firstName']??'',
            lastName: $agentData['lastName']??'',
            email: $agentData['email']??'',
        );
    }
}
