<?php

namespace Application\DTO;

use Domain\Entity\CustomerRequest;

class CustomerRequestEntityDTO
{
    public function __construct(
        public readonly string  $id,
        public readonly string $name,
        public readonly string $energy,
        public readonly AgentDTO $agent
    ) {
    }

    public static function createFromEntity(CustomerRequest $customerRequest, AgentDTO $agentDTO): self
    {
        return new self(
            id:$customerRequest->getId(),
            name:$customerRequest->getName(),
            energy:$customerRequest->getEnergy(),
            agent:$agentDTO
        );
    }
}
