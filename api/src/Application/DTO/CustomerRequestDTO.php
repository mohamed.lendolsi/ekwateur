<?php

namespace Application\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class CustomerRequestDTO
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(min: 3, max: 50)]
        public readonly string $name,
        #[Assert\NotBlank]
        #[Assert\Type(
            type: 'digit',
            message: 'The value {{ value }} is not a valid {{ type }}.',
        )]
        #[Assert\GreaterThanOrEqual(1)]
        public readonly string $agent,
        #[Assert\NotBlank]
        #[Assert\GreaterThanOrEqual(1)]
        public readonly string $energy,
        #[Assert\All([
            new Assert\Collection([
                'reference' => new Assert\Type(
                    type: 'digit',
                    message: 'The value {{ value }} is not a valid {{ type }}.',
                )
            ]),
        ])]
        public array $pointOfDeliveries,
    ) {
    }

    public function getAgentId(): int
    {
        return (int)$this->agent;
    }
}
