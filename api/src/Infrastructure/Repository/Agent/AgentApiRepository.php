<?php

namespace Infrastructure\Repository\Agent;

use Application\DTO\AgentDTO;
use Infrastructure\ApiClient\EkwateurClient\EkwateurAgentApi;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

class AgentApiRepository implements AgentRepositoryInterface
{
    public function __construct(private readonly EkwateurAgentApi $ekwateurAgentApi)
    {
    }

    public function findAgentById(int $id): ?AgentDTO
    {
        return AgentDTO::createFormApiData($this->ekwateurAgentApi->getAgentById($id));
    }
}
