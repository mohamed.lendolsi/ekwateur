<?php

namespace Infrastructure\Repository\Agent;

use Application\DTO\AgentDTO;

interface AgentRepositoryInterface
{
    public function findAgentById(int $id): ?AgentDTO;
}
