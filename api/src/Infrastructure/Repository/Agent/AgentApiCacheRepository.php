<?php

namespace Infrastructure\Repository\Agent;

use Application\DTO\AgentDTO;
use Application\Service\Decorator\HttpClientCacheDecorator;

class AgentApiCacheRepository implements AgentRepositoryInterface
{
    public function __construct(private readonly HttpClientCacheDecorator $ekwateurAgentApi)
    {
    }

    public function findAgentById(int $id): ?AgentDTO
    {
        return AgentDTO::createFormApiData($this->ekwateurAgentApi->request($id));
    }
}
