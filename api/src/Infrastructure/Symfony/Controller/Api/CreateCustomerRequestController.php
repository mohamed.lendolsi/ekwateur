<?php

namespace Infrastructure\Symfony\Controller\Api;

use Application\DTO\CustomerRequestDTO;
use Application\UseCase\CustomerRequest\CreateCustomerRequestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Domain\Entity\CustomerRequest;

class CreateCustomerRequestController extends AbstractController
{
    public function __construct(
        private readonly CreateCustomerRequestHandler $createCustomerRequestHandler,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/customerRequest', methods: ['POST'])]
    public function __invoke(
        #[MapRequestPayload] CustomerRequestDTO $customerRequestDTO
    ): Response {
        $customerRequest =  $this->createCustomerRequestHandler->handle($customerRequestDTO);

        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('customerRequest')
            ->toArray();

        return new Response($this->serializer->serialize($customerRequest, 'json', $context), '200');
    }
}
