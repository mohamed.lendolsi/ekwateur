<?php

namespace Infrastructure\Symfony\Controller\Api;

use Application\DTO\CustomerRequestDTO;
use Application\UseCase\CustomerRequest\CreateCustomerRequestHandler;
use Application\UseCase\CustomerRequest\GetCustomerRequestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Domain\Entity\CustomerRequest;

class GetCustomerRequestController extends AbstractController
{
    public function __construct(
        private readonly GetCustomerRequestHandler $getCustomerRequestHandler,
        private readonly SerializerInterface $serializer
    ) {
    }

    #[Route('/customerRequest/{id}')]
    #[ParamConverter('customerRequest', class: 'Domain\Entity\CustomerRequest')]
    public function __invoke(
        CustomerRequest $customerRequest
    ): Response {
        $customerRequestDto = $this->getCustomerRequestHandler->handle($customerRequest);

        return new Response($this->serializer->serialize($customerRequestDto, 'json'), '200');
    }
}
