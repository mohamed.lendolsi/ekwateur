<?php

namespace Infrastructure\Symfony\Controller\Api;

use Application\Service\Messenger\Handler\PointOfDeliveryMessageHandler;
use Application\Service\Messenger\Message\SmsNotification;
use Domain\Entity\PointOfDelivery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Application\Service\Messenger\Message\PointOfDeliveryMessage;

class SendPointOfDeliveryDataController
{
    #[Route('/pointDeliveryData', methods: ['POST'])]
    public function __invoke(
        MessageBusInterface $bus,
        Request $request
    ): Response {
        $bus->dispatch(new PointOfDeliveryMessage($request->getContent()));

        return new JsonResponse('Done', 200);
    }
}
