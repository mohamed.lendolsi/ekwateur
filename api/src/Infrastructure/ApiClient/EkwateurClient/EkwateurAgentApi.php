<?php

namespace Infrastructure\ApiClient\EkwateurClient;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class EkwateurAgentApi extends AbstractEkwateurClient
{
    protected const HTTP_METHODE = 'GET';

    protected const URI = 'agents';

    public function __construct(HttpClientInterface $ekwateurHttpClient)
    {
        parent::__construct($ekwateurHttpClient);
    }

    public function getAgentById(int $agentId): array
    {
        $response = $this->callApi($agentId);

        if (200 !== $response->getStatusCode()) {
            throw new \Exception();
        }

        return $response->toArray();
    }

    public function getOptions(): string
    {
        return '';
    }

    public function getHttpMethode(): string
    {
        return self::HTTP_METHODE;
    }

    public function getUri(): string
    {
        return self::URI;
    }
}
