<?php

namespace Infrastructure\ApiClient\EkwateurClient;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class AbstractEkwateurClient implements EkwateurClientInterface
{
    public function __construct(private readonly HttpClientInterface $ekwateurHttpClient)
    {
    }

    public function callApi(?int $id = null): ResponseInterface
    {
        $uri = $this->getUri();
        if(null !== $id) {
            $uri = sprintf('%s/%d', $uri, $id);
        }

        return $this->ekwateurHttpClient->request($this->getHttpMethode(), sprintf('%s?%s', $uri, $this->getOptions()));
    }
}
