<?php

namespace Infrastructure\ApiClient\EkwateurClient;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface EkwateurClientInterface
{
    public function callApi(?int $id): ResponseInterface;

    public function getOptions(): string;

    public function getHttpMethode(): string;

    public function getUri(): string;

}
