<?php

namespace Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity()]
class PointOfDelivery
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;


    #[ORM\Column(length: 255)]
    #[Groups(['customerRequest'])]
    private string $reference;

    #[ORM\Column(length: 255, nullable:true)]
    private string $inseeCode;

    #[ORM\Column(length: 255, nullable:true)]
    private string $city;

    #[ORM\Column(length: 255, nullable:true)]
    private string $address;

    #[ORM\ManyToOne(targetEntity: CustomerRequest::class)]
    private CustomerRequest $customerRequest;

    public function getId(): string
    {
        return $this->id;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): PointOfDelivery
    {
        $this->reference = $reference;
        return $this;
    }

    public function getInseeCode(): string
    {
        return $this->inseeCode;
    }

    public function setInseeCode(string $inseeCode): PointOfDelivery
    {
        $this->inseeCode = $inseeCode;
        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): PointOfDelivery
    {
        $this->city = $city;
        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): PointOfDelivery
    {
        $this->address = $address;
        return $this;
    }

    public function getCustomerRequest(): CustomerRequest
    {
        return $this->customerRequest;
    }

    public function setCustomerRequest(?CustomerRequest $customerRequest): PointOfDelivery
    {
        $this->customerRequest = $customerRequest;
        return $this;
    }
}
