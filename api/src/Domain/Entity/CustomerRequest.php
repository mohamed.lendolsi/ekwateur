<?php

namespace Domain\Entity;

use Application\DTO\CustomerRequestDTO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Uid\Uuid as Uuid;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity()]
class CustomerRequest
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(['customerRequest'])]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['customerRequest'])]
    private string $name;

    #[ORM\Column(length: 255)]
    #[Groups(['customerRequest'])]
    private string $agent;

    #[ORM\Column(length: 255)]
    #[Groups(['customerRequest'])]
    private string $energy;

    #[ORM\OneToMany(targetEntity: PointOfDelivery::class, mappedBy: 'customerRequest', cascade: ['persist', 'remove'])]
    private Collection $pointOfDeliveries;

    /**
     * @param Collection $pointOfDelivery
     */
    public function __construct()
    {
        $this->pointOfDeliveries= new ArrayCollection();
    }

    public static function createFromDto(CustomerRequestDTO $customerRequestDTO)
    {
        $customerRequest = new self();
        $customerRequest->setName($customerRequestDTO->name)
            ->setEnergy($customerRequestDTO->energy)
            ->setAgent($customerRequestDTO->getAgentId());

        foreach ($customerRequestDTO->pointOfDeliveries as $pointOfDelivery) {
            $pointOfDeliveryReference = (new PointOfDelivery())->setReference($pointOfDelivery['reference']);
            $customerRequest->addPointOfDelivery($pointOfDeliveryReference);
        }
        return $customerRequest;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CustomerRequest
    {
        $this->name = $name;
        return $this;
    }

    public function getAgent(): string
    {
        return $this->agent;
    }

    public function setAgent(string $agent): CustomerRequest
    {
        $this->agent = $agent;
        return $this;
    }

    public function getEnergy(): string
    {
        return $this->energy;
    }

    public function setEnergy(string $energy): CustomerRequest
    {
        $this->energy = $energy;
        return $this;
    }

    public function getPointOfDelivery()
    {
        return $this->pointOfDelivery;
    }

    public function setPointOfDelivery(PointOfDelivery $pointOfDelivery)
    {
        $this->pointOfDelivery = $pointOfDelivery;
        return $this;
    }
    public function addPointOfDelivery(PointOfDelivery $pointOfDelivery): void
    {
        $pointOfDelivery->setCustomerRequest($this); // synchronously updating inverse side
        $this->pointOfDeliveries [] = $pointOfDelivery;
    }
}
