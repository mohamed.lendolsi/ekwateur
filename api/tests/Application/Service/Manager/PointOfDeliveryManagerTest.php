<?php

namespace Application\Service\Manager;

use Application\DTO\PointOfDeliveryAsyncDTO;
use Doctrine\ORM\EntityManagerInterface;
use Domain\Entity\PointOfDelivery;
use PHPUnit\Framework\TestCase;

class PointOfDeliveryManagerTest extends TestCase
{
    public function testUpdatePOD()
    {
        $pointOfDeliveryAsyncDTO = new PointOfDeliveryAsyncDTO('123456', '123456', 'Test City', 'Test Address');
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $pointOfDelivery = $this->createMock(PointOfDelivery::class);
        $repository = $this->createMock(\Doctrine\Persistence\ObjectRepository::class);

        $entityManager->expects($this->once())
            ->method('getRepository')
            ->with(PointOfDelivery::class)
            ->willReturn($repository);

        $repository->expects($this->once())
            ->method('findBy')
            ->with(['reference' => $pointOfDeliveryAsyncDTO->reference])
            ->willReturn([$pointOfDelivery]);

        $pointOfDelivery->expects($this->once())
            ->method('setInseeCode')
            ->with($pointOfDeliveryAsyncDTO->inseeCode)
            ->willReturnSelf();

        $pointOfDelivery->expects($this->once())
            ->method('setCity')
            ->with($pointOfDeliveryAsyncDTO->city)
            ->willReturnSelf();

        $pointOfDelivery->expects($this->once())
            ->method('setAddress')
            ->with($pointOfDeliveryAsyncDTO->address)
            ->willReturnSelf();

        $entityManager->expects($this->once())
            ->method('persist')
            ->with($pointOfDelivery);

        $entityManager->expects($this->once())
            ->method('flush');

        $pointOfDeliveryManager = new PointOfDeliveryManager($entityManager);

        $pointOfDeliveryManager->updatePOD($pointOfDeliveryAsyncDTO);
    }
}
