<?php

namespace Infrastructure\Symfony\Controller\Api;

use Application\UseCase\CustomerRequest\GetCustomerRequestHandler;
use Domain\Entity\CustomerRequest;
use Infrastructure\Symfony\Controller\Api\GetCustomerRequestController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
class GetCustomerRequestControllerTest extends TestCase
{
    public function testInvoke()
    {
        $customerRequest = $this->createMock(CustomerRequest::class);
        $customerRequestDto = ['id' => 1, 'name' => 'Test Customer Request'];

        $getCustomerRequestHandler = $this->createMock(GetCustomerRequestHandler::class);
        $serializer = $this->createMock(SerializerInterface::class);

        $getCustomerRequestHandler->expects($this->once())
            ->method('handle')
            ->with($customerRequest)
            ->willReturn($customerRequestDto);

        $serializer->expects($this->once())
            ->method('serialize')
            ->with($customerRequestDto, 'json')
            ->willReturn('{"id": 1, "name": "Test Customer Request"}');

        $controller = new GetCustomerRequestController($getCustomerRequestHandler, $serializer);

        $request = Request::create('/customerRequest/1', 'GET');

        $response = $controller->__invoke($customerRequest);

        $this->assertEquals('{"id": 1, "name": "Test Customer Request"}', $response->getContent());

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
