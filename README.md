

<div align="center">
      Candidate Mohamed Lendolsi
</div>

## Requirements
- Docker
- Docker compose

# Install

Please follow the steps depending on your operating system.

### Install Linux
- Clone project
- In the project folder run:
  ```sh
  make install
  ```

### Install Windows:
@todo with NMAKE tool


# Execute command:
### Linux
- Unit Tests :
  ```sh
  make unit-test
    ```

# Application:
### Create the `CustomerRequest`
- curl in Host computer  :
```console
curl --location '127.0.0.1/api/customerRequest' \
--header 'Content-Type: application/json' \
--data '{
    "name": "A new customer request",
    "agent": "2",
    "energy": "ELECTRICITY",
    "pointOfDeliveries": [
        {
            "reference": "12345678910111"
        },
        {
            "reference": "12345678910112"
        }
    ]
}'
```

###  Get the `CustomerRequest`
- curl in Host computer  :
```console
curl --location '127.0.0.1/api/customerRequest/0188d7a3-c9eb-7516-9c7d-cf727b5e7953'
```

###  Create Asyn Payload
- curl in Host computer  :
```console
curl --location '127.0.0.1/api/pointDeliveryData' \
--header 'Content-Type: application/json' \
--data '{
  "reference": 12345678910111,
  "inseeCode": 75009,
  "city": "Paris",
  "address": "79 Rue de Clichy"
}'
```
And

```console
make consumer-run
```

# ToDo : 
- behavior test x
- Api Documentation x
- Mapping Orm with YML files x
- Custom Exception x
-... 
