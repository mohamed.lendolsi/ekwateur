DC=docker-compose
CONTAINER=php
EXEC=$(DC) exec $(CONTAINER) /bin/bash
EXEC_REACT=$(DC) exec react /bin/bash

install: up vendor doctrine

up:
	$(DC) up -d

vendor:
	$(EXEC) -c 'composer install --no-suggest --no-progress'

doctrine:
	$(EXEC) -c 'bin/console doctrine:database:create '
	$(EXEC) -c 'bin/console doctrine:schema:update --force'

unit-test:
	$(EXEC) -c 'php ./vendor/bin/phpunit'

consumer-run:
	$(EXEC) -c 'php bin/console messenger:consume async -vv'

